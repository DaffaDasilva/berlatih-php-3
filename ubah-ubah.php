<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<h1>Merubah Huruf</h1>
<a href="index.html">HOME</a><br>

<?php
function ubah_huruf($string){
//kode di sini
    $huruf = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z',];
    $jawaban = "";

    for($i = 0; $i < strlen($string); $i++){
        for($x = 0; $x < count($huruf); $x++){
            if($huruf[$x] === $string[$i]){
                $jawaban .= $huruf[$x + 1];
            }
        }
    }
    echo $string." : ". $jawaban."<br/>";
}


// TEST CASES
echo ubah_huruf('wow'); // xpx
echo ubah_huruf('developer'); // efwfmpqfs
echo ubah_huruf('laravel'); // mbsbwfm
echo ubah_huruf('keren'); // lfsfo
echo ubah_huruf('semangat'); // tfnbohbu

?>
</body>
</html>