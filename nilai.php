<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<h1>Tentukan Nilai</h1>
<a href="index.html">HOME</a><br>
<?php
function tentukan_nilai($number)
{
    //  kode disini
    if($number >= 85 && $number <= 100){
        return $number." : Sangat Baik<br/>";
    } else
    if($number >= 70 && $number <= 84){
        return $number." : Baik<br/>";
    } else
    if($number >= 60 && $number <= 69){
        return $number." : Cukup<br/>";
    } else
    if($number >= 0 && $number <= 59){
        return $number." : Kurang<br/>";
    } else{
        return $number." : Tidak terdefinisi<br/>";
    }
}

//TEST CASES
echo tentukan_nilai(98); //Sangat Baik
echo tentukan_nilai(76); //Baik
echo tentukan_nilai(67); //Cukup
echo tentukan_nilai(43); //Kurang
?>
</body>
</html>