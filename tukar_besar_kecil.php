<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Tukar Besar Kecil</h1>
    <a href="index.html">HOME</a><br>
<?php
function tukar_besar_kecil($string){
//kode di sini
    $uplow = "";
    for($a = 0; $a < strlen($string); $a++){
        if(preg_match('~^\p{Lu}~u', $string[$a])){
            $uplow .= strtolower($string[$a]);
        } else{
            $uplow .= strtoupper($string[$a]);
        }
    }
    return $string." : ".$uplow."<br/>";
}

// TEST CASES
echo tukar_besar_kecil('Hello World'); // "hELLO wORLD"
echo tukar_besar_kecil('I aM aLAY'); // "i Am Alay"
echo tukar_besar_kecil('My Name is Bond!!'); // "mY nAME IS bOND!!"
echo tukar_besar_kecil('IT sHOULD bE me'); // "it Should Be ME"
echo tukar_besar_kecil('001-A-3-5TrdYW'); // "001-a-3-5tRDyw"

?>
</body>
</html>